from django.contrib import admin
from .models import Specie


class SpecieAdmin(admin.ModelAdmin):
    list_display = ('specie','is_active')
    search_fields = ('specie',)
    list_per_page = 10
    list_filter = ('is_active',)



admin.site.register(Specie,SpecieAdmin)