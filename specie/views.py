from datetime import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Specie
from animal.models import Animal
from vet.models import Vet
from incompatibility.models import Incompatibility
from .serializers import SpecieSerializer
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class GetAllSpecies(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticatedVet]
    queryset = Specie.objects.filter(is_active=True)
    allowed_methods = ('GET', 'POST')
    serializer_class = SpecieSerializer    

    def get(self, request, *args, **kwargs):
        self.serializer_class = SpecieSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='Ok, created'

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)


        for data in ['specie']:
            if not data in request.data:
                return Response({"Some attributes were not provided"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)
        
        try:
            specie = Specie(
                specie=request.data.get('specie'))

            specie.full_clean()
            specie.save()
             
        except:
            msg = 'The attribute is invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class GetSpecie(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SpecieSerializer
    queryset = Specie.objects.filter(is_active=True)
    allowed_methods = ('GET', 'DELETE')
    lookup_field = 'id'

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = SpecieSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Specie.objects.get(id = id, is_active=True)
        except:
            return Response({"The requested specie does not exist"}, status=status.HTTP_400_BAD_REQUEST)
        
  
        return super().get(request, *args, **kwargs) 

    def delete(self, request, id):

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        specie = self.queryset.get(id=id)

        try: 
            relatedAnimals=Animal.objects.get(specie=specie, is_active=True)
            return Response({"You can not delete a specie with related animals, please, delete them first"}, status=status.HTTP_409_CONFLICT)
        except:   
            try:
                relatedIncompatibilities=Incompatibility.objects.get(specie=specie, is_active=True)
                return Response({"You can not delete a specie with related imcompatibilities, please, delete them first"}, status=status.HTTP_409_CONFLICT)
            except:
                specie.is_active = False
                specie.save() 

        return Response(status=status.HTTP_204_NO_CONTENT)


       



                


