from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('specie/', views.GetAllSpecies.as_view(), name='specie'),
    path('specie/<id>/', views.GetSpecie.as_view(), name='specie2'),
]