from rest_framework import serializers
from .models import Specie


class SpecieSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Specie
        fields = ('id','specie')



