from rest_framework import serializers
from .models import Vaccine
from animal.models import Animal
from animal.serializers import AnimalSerializer


class VaccineSerializer(serializers.HyperlinkedModelSerializer):
    animal = AnimalSerializer(many=False)

    class Meta:
        model = Vaccine
        fields = ('id', 'moment', 'name', 'description', 'animal')

class AnimalForVaccineSerializerForPost(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Animal
        fields = ('identificator',)

class VaccineSerializerForPost(serializers.HyperlinkedModelSerializer):
    animal = AnimalForVaccineSerializerForPost(many=False)

    class Meta:
        model = Vaccine
        fields = ('name', 'description', 'animal')

