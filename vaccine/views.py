from datetime import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Vaccine
from animal.models import Animal
from vet.models import Vet
from .serializers import VaccineSerializer
from .serializers import VaccineSerializerForPost
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class GetAllVaccines(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticatedVet]
    queryset = Vaccine.objects.filter(is_active=True)
    allowed_methods = ('GET', 'POST')
    serializer_class = VaccineSerializer    

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return VaccineSerializer
        else:
            return VaccineSerializerForPost

    def get(self, request, *args, **kwargs):
        self.serializer_class = VaccineSerializer

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='Ok, created'

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        
        for data in ['name', 'animal']:
            if not data in request.data:
                return Response({"Some attributes were not provided"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)
        
        try: 
            animalIn=request.data.get('animal')
            animalId=animalIn['identificator']
            animal = Animal.objects.get(identificator = animalId, is_active=True)  
        except:   
            return Response({"Invalid animal"}, status=status.HTTP_400_BAD_REQUEST) 
        

        try:
            vaccine = Vaccine(
                moment=datetime.now(), 
                name=request.data.get('name'),
                description=request.data.get('description'),
                animal=animal)

            vaccine.full_clean()
            vaccine.save()
             
        except:
            msg = 'Some attributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class GetVaccine(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = VaccineSerializer
    queryset = Vaccine.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return VaccineSerializer
        else:
            return VaccineSerializerForPost

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = VaccineSerializer

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Vaccine.objects.get(id = id, is_active=True)
        except:
            return Response({"Invalid Vaccine"}, status=status.HTTP_400_BAD_REQUEST)

        return super().get(request, *args, **kwargs) 

    # def delete(self, request, id):

    #     # try: 
    #     #     accountId=request.user.id
    #     #     admin=Admin.objects.get(user=accountId)
    #     # except:   
    #     #     return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

    #     vaccine = self.queryset.get(id=id)
    #     vaccine.is_active = False
    #     vaccine.save()

    #     return Response(status=status.HTTP_204_NO_CONTENT)

class VaccinesByAnimal(generics.ListCreateAPIView):
    serializer_class = VaccineSerializer
    queryset = Vaccine.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return VaccineSerializer
        else:
            return VaccineSerializerForPost

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = VaccineSerializer

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            animal = Animal.objects.get(id = id, is_active=True)
        except:
            return Response({"Invalid Animal"}, status=status.HTTP_400_BAD_REQUEST)

        
        self.queryset = Vaccine.objects.filter(animal = animal, is_active=True)
  
        return super().get(request, *args, **kwargs)  


       



                


