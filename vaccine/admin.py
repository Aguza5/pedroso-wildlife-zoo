from django.contrib import admin
from .models import Vaccine


class VaccineAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('name','get_animal','moment','is_active')
    search_fields = ('name','animal__name',)
    list_per_page = 10
    list_filter = ('is_active',)

    
    def get_animal(self, obj):
        return obj.animal.name

    get_animal.short_description = 'Animal'


admin.site.register(Vaccine,VaccineAdmin)