from django.db import models
from django.core.exceptions import ValidationError
from animal.models import Animal
from django.core.validators import MinLengthValidator



class Vaccine(models.Model):
    moment = models.DateField(('Moment'),blank=False, null=False)
    name = models.CharField(('Name'),max_length=50,blank=False, null=False, validators=[MinLengthValidator(5)])
    description = models.CharField(('Description'),max_length=200,blank=True, null=True, validators=[MinLengthValidator(5)])
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    animal = models.ForeignKey('animal.Animal', related_name='realtedAnimalVaccine', on_delete=models.CASCADE)

    def __str__(self):
       return self.name
   
        
            
  




    
    
       
       