from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('vaccine/', views.GetAllVaccines.as_view(), name='vaccine'),
    path('vaccine/<id>/', views.GetVaccine.as_view(), name='vaccine2'),
    path('vaccine/animal/<id>/', views.VaccinesByAnimal.as_view(), name='vaccine3'),
]