from django.db import models

class Vet(models.Model):

    user = models.ForeignKey('auth.User', related_name='vetrelatedUseraccount', on_delete=models.CASCADE)

    def __str__(self):
       return self.user.username
