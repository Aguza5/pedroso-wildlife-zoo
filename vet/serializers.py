from rest_framework import serializers
from .models import Vet
from django.contrib.auth.models import User




class userSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'password', 'email','first_name','last_name')


class VetSerializer(serializers.HyperlinkedModelSerializer):

    user = userSerializer(many=False)

    class Meta:
        model = Vet
        fields = ('user',)


