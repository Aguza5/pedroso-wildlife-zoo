from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .isAuthenticatedVet import IsAuthenticatedVet
from .models import Vet
from rest_framework import generics, status, viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response
from .serializers import VetSerializer



class ShowYourAccount(viewsets.ReadOnlyModelViewSet):
    """
    This is the information storage in Pedroso Wildlife Zoo Data Base

    """
    permission_classes = [IsAuthenticatedVet]
    serializer_class = VetSerializer
    queryset=[]
    
    def list(self, request, *args, **kwargs):
        userid= self.request.user.id
        queryset= Vet.objects.get(user__id = userid)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)