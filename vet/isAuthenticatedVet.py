from rest_framework import permissions
from .models import Vet

class IsAuthenticatedVet (permissions.BasePermission):
    def has_permission(self, request, view):
    
        user_id = request.user.id
        is_vet=True
        try:
            Vet.objects.get(user=user_id)
        except:
            is_vet=False

        return is_vet