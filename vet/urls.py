from . import views
from django.urls import path

urlpatterns = [
    path('vetshow/', views.ShowYourAccount.as_view({'get': 'list'}), name='VetShow'),
]