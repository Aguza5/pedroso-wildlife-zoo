from django.db import models
from django.core.exceptions import ValidationError
from animal.models import Animal
from django.core.validators import MinLengthValidator



class MedicalReport(models.Model):
    moment = models.DateField(('Moment'),blank=False, null=False)
    diagnosis = models.CharField(('Diagnosis'),max_length=200,blank=False, null=False, validators=[MinLengthValidator(5)])
    treatment = models.CharField(('Treatment'),max_length=200,blank=True, null=True, validators=[MinLengthValidator(5)])
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    animal = models.ForeignKey(Animal, related_name='ReledAnimalMedicalReport', on_delete=models.CASCADE)

    def __str__(self):
       return 'Medical report of: {0}'.format(self.animal)
   
        
            
  




    
    
       
       