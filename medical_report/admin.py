from django.contrib import admin
from .models import MedicalReport


class MedicalReportAdmin(admin.ModelAdmin):
    list_display = ('animal','moment','is_active')
    search_fields = ('animal__name',)
    list_per_page = 10
    list_filter = ('is_active','animal__name')



admin.site.register(MedicalReport,MedicalReportAdmin)