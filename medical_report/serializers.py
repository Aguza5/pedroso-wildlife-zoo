from rest_framework import serializers
from .models import MedicalReport
from animal.models import Animal
from animal.serializers import AnimalSerializer


class MedicalReportSerializer(serializers.HyperlinkedModelSerializer):
    animal = AnimalSerializer(many=False)

    class Meta:
        model = MedicalReport
        fields = ('id', 'moment', 'diagnosis', 'treatment', 'animal')

class AnimalForMedicalReportSerializerForPost(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Animal
        fields = ('identificator',)

class MedicalReportSerializerForPost(serializers.HyperlinkedModelSerializer):
    animal = AnimalForMedicalReportSerializerForPost(many=False)

    class Meta:
        model = MedicalReport
        fields = ('diagnosis', 'treatment', 'animal')