from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('medical_report/', views.GetAllMedicalReports.as_view(), name='medicalReports'),
    path('medical_report/<id>/', views.GetMedicalReport.as_view(), name='medicalReports2'),
    path('medical_report/animal/<id>/', views.MedicalReportsByAnimal.as_view(), name='medicalReports3'),
]