from django.apps import AppConfig


class MedicalReportConfig(AppConfig):
    name = 'MedicalReport'
