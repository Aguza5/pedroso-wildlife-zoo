from datetime import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.response import Response
from .models import MedicalReport
from animal.models import Animal
from vet.models import Vet
from .serializers import MedicalReportSerializer
from .serializers import MedicalReportSerializerForPost
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from vet.isAuthenticatedVet import IsAuthenticatedVet


class GetAllMedicalReports(generics.ListCreateAPIView):
    queryset = MedicalReport.objects.filter(is_active=True)
    allowed_methods = ('GET', 'POST')
    serializer_class = MedicalReportSerializer    

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MedicalReportSerializer
        else:
            return MedicalReportSerializerForPost

    def get(self, request, *args, **kwargs):
        self.serializer_class = MedicalReportSerializer

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='Ok, created'
        
        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)


        for data in ['diagnosis', 'animal']:
            if not data in request.data:
                return Response({"Some attributes were not provided"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)
        
        try: 
            animalIn=request.data.get('animal')
            animalId=animalIn['identificator']
            animal = Animal.objects.get(identificator = animalId, is_active=True)  
        except:   
            return Response({"Invalid animal"}, status=status.HTTP_400_BAD_REQUEST) 
        

        try:
            medicalReport = MedicalReport(
                moment=datetime.now(), 
                diagnosis=request.data.get('diagnosis'),
                treatment=request.data.get('treatment'),
                animal=animal)

            medicalReport.full_clean()
            medicalReport.save()
             
        except:
            msg = 'Some attributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class GetMedicalReport(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = MedicalReportSerializer
    queryset = MedicalReport.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MedicalReportSerializer
        else:
            return MedicalReportSerializerForPost

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = MedicalReportSerializer

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = MedicalReport.objects.get(id = id, is_active=True)
        except:
            return Response({"The requested medical report does not exist"}, status=status.HTTP_400_BAD_REQUEST)
  
        return super().get(request, *args, **kwargs) 

    # def delete(self, request, id):

    #     # try: 
    #     #     accountId=request.user.id
    #     #     admin=Admin.objects.get(user=accountId)
    #     # except:   
    #     #     return Response({}, status=status.HTTP_401_UNAUTHORIZED)

    #     medicalReport = self.queryset.get(id=id)
    #     medicalReport.is_active = False
    #     medicalReport.save()

    #     return Response(status=status.HTTP_204_NO_CONTENT)

class MedicalReportsByAnimal(generics.ListCreateAPIView):
    serializer_class = MedicalReportSerializer
    queryset = MedicalReport.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    # def get_serializer_class(self):
    #     if self.request.method == 'GET':
    #         return MedicalReportSerializer
    #     else:
    #         return MedicalReportSerializerForPost

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = MedicalReportSerializer

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            animal = Animal.objects.get(id = id, is_active=True)
        except:
            return Response({"Invalid Animal"}, status=status.HTTP_400_BAD_REQUEST)

        self.queryset = MedicalReport.objects.filter(animal = animal)
  
        return super().get(request, *args, **kwargs) 


       



                


