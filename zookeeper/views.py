from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .isAuthenticatedZookeeper import IsAuthenticatedZookeeper
from .models import Zookeeper
from rest_framework import generics, status, viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response
from .serializers import ZookeeperSerializer



class ShowYourAccount(viewsets.ReadOnlyModelViewSet):
    """
    This is the information storage in Pedroso Wildlife Zoo Data Base

    """
    permission_classes = [IsAuthenticatedZookeeper]
    serializer_class = ZookeeperSerializer
    queryset=[]
    
    def list(self, request, *args, **kwargs):
        userid= self.request.user.id
        queryset= Zookeeper.objects.get(user__id = userid)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)