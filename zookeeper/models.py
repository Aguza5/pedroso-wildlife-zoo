from django.db import models

class Zookeeper(models.Model):

    user = models.ForeignKey('auth.User', related_name='relatedUseraccount', on_delete=models.CASCADE)

    def __str__(self):
       return self.user.username
