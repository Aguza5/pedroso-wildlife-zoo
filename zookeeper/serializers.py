from rest_framework import serializers
from .models import Zookeeper
from django.contrib.auth.models import User




class userSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'password', 'email','first_name','last_name')


class ZookeeperSerializer(serializers.HyperlinkedModelSerializer):

    user = userSerializer(many=False)

    class Meta:
        model = Zookeeper
        fields = ('user',)


