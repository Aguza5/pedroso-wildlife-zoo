from rest_framework import permissions
from .models import Zookeeper

class IsAuthenticatedZookeeper (permissions.BasePermission):
    def has_permission(self, request, view):
    
        user_id = request.user.id
        is_zookeeper=True
        try:
            Zookeeper.objects.get(user=user_id)
        except:
            is_zookeeper=False

        return is_zookeeper