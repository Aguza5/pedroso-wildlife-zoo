from . import views
from django.urls import path
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
    path('zookeepershow/', views.ShowYourAccount.as_view({'get': 'list'}), name='ZookeeperShow'),
]