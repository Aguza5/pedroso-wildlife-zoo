from django.contrib import admin
from django.urls import include,path
from rest_framework_simplejwt import views as jwt_views
from django.conf.urls import url



urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include('customer.urls')),
    url(r'^', include('medical_report.urls')),
    url(r'^', include('specie.urls')),
    url(r'^', include('animal.urls')),
    url(r'^', include('vaccine.urls')),
    url(r'^', include('incompatibility.urls')),
    url(r'^', include('control.urls')),
    url(r'^', include('enclosure.urls')),
    url(r'^', include('zookeeper.urls')),
    url(r'^', include('administrator.urls')),
    url(r'^', include('vet.urls')),

    # AUTH
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api-auth/', include('rest_framework.urls')),

]

admin.site.site_header = "Pedroso Wildlife admin site"
admin.site.site_title = "Pedroso Wildlife admin site"
admin.site.index_title = "Welcome to Pedroso Wildlife admin site"