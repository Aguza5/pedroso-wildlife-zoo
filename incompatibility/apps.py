from django.apps import AppConfig


class IncompatibilityConfig(AppConfig):
    name = 'incompatibility'
