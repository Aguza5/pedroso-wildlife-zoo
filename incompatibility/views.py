from datetime import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from django.db.models import Q
from rest_framework.response import Response
from .models import Incompatibility
from animal.models import Animal
from vet.models import Vet
from specie.models import Specie
from administrator.models import Administrator
from .serializers import IncompatibilitySerializer
from .serializers import IncompatibilitySerializerForPost
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class GetAllIncompatibilities(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticatedVet]
    queryset = Incompatibility.objects.filter(is_active=True)
    allowed_methods = ('GET', 'POST')
    serializer_class = IncompatibilitySerializer    

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return IncompatibilitySerializer
        else:
            return IncompatibilitySerializerForPost

    def get(self, request, *args, **kwargs):
        self.serializer_class = IncompatibilitySerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='Ok, created'

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        
        for data in ['specie1', 'specie2']:
            if not data in request.data:
                return Response({"Some attributes were not provided"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)
        
        try: 
            specieIn=request.data.get('specie1')
            specieId=specieIn['specie']
            specie = Specie.objects.get(specie = specieId, is_active=True)  
        except:   
            return Response({"Invalid specie1"}, status=status.HTTP_400_BAD_REQUEST) 

        try: 
            specieIn2=request.data.get('specie2')
            specieId2=specieIn2['specie']
            specie2 = Specie.objects.get(specie = specieId2, is_active=True)  
        except:   
            return Response({"Invalid specie2"}, status=status.HTTP_400_BAD_REQUEST) 

        if specie==specie2:
            return Response({"The species must be different"}, status=status.HTTP_400_BAD_REQUEST)
        

        try:
            incompatibility = Incompatibility(
                specie1=specie, 
                specie2=specie2)

            incompatibility.full_clean()
            incompatibility.save()
             
        except:
            msg = 'Some attributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class GetIncompatibility(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = IncompatibilitySerializer
    queryset = Incompatibility.objects.filter(is_active=True)
    allowed_methods = ('GET','DELETE')
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return IncompatibilitySerializer
        else:
            return IncompatibilitySerializerForPost

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = IncompatibilitySerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Incompatibility.objects.get(id = id, is_active=True)
        except:
            return Response({"The requested incompatibility does not exist"}, status=status.HTTP_400_BAD_REQUEST)
  
        return super().get(request, *args, **kwargs) 

    def delete(self, request, id):

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        incompatibility = self.queryset.get(id=id)
        incompatibility.is_active = False
        incompatibility.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

class IncompatibilitiesBySpecie(generics.ListCreateAPIView):
    serializer_class = IncompatibilitySerializer
    queryset = Incompatibility.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return IncompatibilitySerializer
        else:
            return IncompatibilitySerializerForPost

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = IncompatibilitySerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            specie = Specie.objects.get(id = id, is_active=True)
        except:
            return Response({"Invalid Specie"}, status=status.HTTP_400_BAD_REQUEST)

        self.queryset = Incompatibility.objects.filter(Q(specie2 = specie, is_active=True) | Q(specie1 = specie, is_active=True))
        
  
        return super().get(request, *args, **kwargs) 


       



                


