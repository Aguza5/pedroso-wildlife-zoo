from django.contrib import admin
from .models import Incompatibility


class IncompatibilityAdmin(admin.ModelAdmin):
    list_display = ('specie1','specie2','is_active')
    search_fields = ('specie1__specie','specie2__specie',)
    list_per_page = 10
    list_filter = ('is_active',)



admin.site.register(Incompatibility,IncompatibilityAdmin)