from django.db import models
from django.core.exceptions import ValidationError
from specie.models import Specie



class Incompatibility(models.Model):
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    specie1 = models.ForeignKey('specie.Specie', related_name='specie1Incompatibility', on_delete=models.CASCADE)
    specie2 = models.ForeignKey('specie.Specie', related_name='specie2Incompatibility', on_delete=models.CASCADE)

    def __str__(self):
       return 'Specie1: {0}, Specie2: {1}'.format(self.specie1, self.specie2)
   
    class Meta:
        verbose_name = 'Incompatibility'
        verbose_name_plural = 'Incompatibilities'    
            
  




    
    
       
       