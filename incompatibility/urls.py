from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('incompatibility/', views.GetAllIncompatibilities.as_view(), name='incompatibility'),
    path('incompatibility/<id>/', views.GetIncompatibility.as_view(), name='incompatibility2'),
    path('incompatibility/specie/<id>/', views.IncompatibilitiesBySpecie.as_view(), name='incompatibility3'),
]