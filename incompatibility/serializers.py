from rest_framework import serializers
from .models import Incompatibility
from specie.serializers import SpecieSerializer


class IncompatibilitySerializer(serializers.HyperlinkedModelSerializer):
    specie1 = SpecieSerializer(many=False)
    specie2 = SpecieSerializer(many=False)

    class Meta:
        model = Incompatibility
        fields = ('id', 'specie1', 'specie2')

class IncompatibilitySerializerForPost(serializers.HyperlinkedModelSerializer):
    specie1 = SpecieSerializer(many=False)
    specie2 = SpecieSerializer(many=False)

    class Meta:
        model = Incompatibility
        fields = ('id', 'specie1', 'specie2')



