from django.db import models

class Customer(models.Model):

    user = models.ForeignKey('auth.User', related_name='userCustomer', on_delete=models.CASCADE)

    def __str__(self):
       return self.user.username
