from . import views
from django.urls import path
from rest_framework import routers


router = routers.DefaultRouter()

urlpatterns = [
    path('customer/', views.CustomerView.as_view(), name='customerGetPost'),
    path('customerchange/', views.CustomerUpdate.as_view(), name='customerPutDelete'),
    path('customershow/', views.ShowYourAccount.as_view({'get': 'list'}), name='CustomerShow'),
]