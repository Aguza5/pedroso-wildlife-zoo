from rest_framework import serializers
from .models import Customer
from django.contrib.auth.models import User




class userSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'password', 'email','first_name','last_name')


class CustomerSerializer(serializers.HyperlinkedModelSerializer):

    user = userSerializer(many=False)

    class Meta:
        model = Customer
        fields = ('user',)



class userPUTSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id','first_name','last_name',)


class CustomerPUTSerializer(serializers.HyperlinkedModelSerializer):

    user = userPUTSerializer(many=False)

    class Meta:
        model = Customer
        fields = ('user',)        


