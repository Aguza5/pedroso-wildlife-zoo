from rest_framework import permissions
from .models import Customer

class IsAuthenticatedCustomer (permissions.BasePermission):
    def has_permission(self, request, view):
    
        user_id = request.user.id
        is_customer=True
        try:
            Customer.objects.get(user=user_id)
        except:
            is_customer=False

        return is_customer