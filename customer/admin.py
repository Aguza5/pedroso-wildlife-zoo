from django.contrib import admin
from .models import Customer




class CustomerAdmin(admin.ModelAdmin):
    list_display = ('get_user__username','get_user__first_name','get_user__last_name','get_user__is_active')
    search_fields = ('user__username','user__first_name','user__last_name')
    list_per_page = 10
    list_filter = ('user__is_active',)

    def get_user__is_active(self, obj):
        return obj.user.is_active

    get_user__is_active.short_description = 'Is active'
    get_user__is_active.admin_order_field = 'customer__user__is_active'
    get_user__is_active.boolean = True
    
    def get_user__username(self, obj):
        return obj.user.username

    get_user__username.short_description = 'User name'

    def get_user__first_name(self, obj):
        return obj.user.first_name

    get_user__first_name.short_description = 'Fisrt Name'

    def get_user__last_name(self, obj):
        return obj.user.last_name

    get_user__last_name.short_description = 'Last Name'

admin.site.register(Customer, CustomerAdmin)




from django.contrib import  messages
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

# Unregister the provided model admin
admin.site.unregister(User)

@admin.register(User)
class CustomUserAdmin(UserAdmin):
    actions = [
        'activate_users',
    ]

    def banUsers(self, request, queryset):

        for user_account in queryset:
            user_account.is_active=False
            user_account.save()
        msg = 'Users banned correctly'
        self.message_user(request, msg, messages.SUCCESS)    

    banUsers.short_description = "Ban selected users"


    def unbanUsers(self, request, queryset):
        
        
        for user_account in queryset: 
            
            user_account.is_active=True
            user_account.save()
                        
        msg = 'Users unbanned correctly'
        self.message_user(request, msg, messages.SUCCESS) 

    unbanUsers.short_description = "Unban selected users"

    actions = [ banUsers,unbanUsers]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions