from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .isAuthenticatedCustomer import IsAuthenticatedCustomer
from .models import Customer
from rest_framework import generics, status, viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response
from .serializers import CustomerSerializer
from .serializers import CustomerPUTSerializer
from django.db import transaction
from django.shortcuts import get_object_or_404



class CustomerView(generics.ListCreateAPIView):
    """
    
    In this url you can create a new customer. You only should send a JSON with your data. This JSON can be sended with a POST. 
    You can use the form below and the botton POST or use the view Raw Data with your JSON.

    """

    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()


    def get(self, request, *args, **kwargs):
        st = status.HTTP_200_OK
        msg = 'You can register here (with POST)'
        return Response(msg, status=st)

    def post(self, request, *args, **kwargs):
        st=status.HTTP_201_CREATED
        msg='ok, created'

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        try:
            userAccount = request.data.get('user')
            username = userAccount['username']
            password = userAccount['password']
            email = userAccount['email']
            first_name = userAccount['first_name']
            last_name = userAccount['last_name']
        except:
            msg= 'Invalid JSON, the required parameters are: username, password and email'
            st=status.HTTP_400_BAD_REQUEST    
            return Response(msg, status=st)
        

        for data in ['username', 'password', 'email','first_name','last_name']:
            if not data in userAccount:
                msg= 'Invalid JSON, the required parameters are: username, password and email'
                st=status.HTTP_400_BAD_REQUEST    
                return Response(msg, status=st)
        
        with transaction.atomic():
            try:
                

                if (first_name==None or first_name=='') and (last_name==None or last_name==''):
                    user = User.objects.create_user(username=username,
                                                    email=email,
                                                    password=password)
                    
                elif (first_name==None or first_name=='') and (last_name!=None and last_name!='' ):   
                    user = User.objects.create_user(username=username,
                                                    email=email,
                                                    password=password,
                                                    last_name=last_name)
                elif (first_name!=None and first_name!='') and (last_name==None or last_name=='' ):   
                    user = User.objects.create_user(username=username,
                                                    email=email,
                                                    password=password,
                                                    first_name=first_name)
                else:
                    user = User.objects.create_user(username=username,
                                                    email=email,
                                                    password=password,
                                                    first_name=first_name,
                                                    last_name=last_name)                                                       

                user.clean()
                user.full_clean()
                user.save()

                customer = Customer(user=user)
                customer.clean()
                customer.full_clean()
                customer.save()

            except:
                msg= 'Some erros in the attributes are detected'
                st=status.HTTP_422_UNPROCESSABLE_ENTITY   

        return Response(msg, status=st)        

class CustomerUpdate(generics.RetrieveUpdateDestroyAPIView):
    """
    Here you can change your first name and last name with a PUT or delete your account with a DELETE.
    Remember that if you send a first name or last name blanck, they will not change.
    If you only want to change one of these 2 attributes send send the attribute you don't want to change blanck.

    """

    permission_classes = (IsAuthenticatedCustomer,)
    serializer_class = CustomerPUTSerializer
    allowed_methods = ('GET','PUT','DELETE')
    
    def get_queryset(self, *args, **kwargs):
        queryset= []
        return queryset
       

    def get(self, request, *args, **kwargs):
        st = status.HTTP_200_OK
        msg = 'You can update (with PUT) or delete (with DELETE) your account here. If you want change your account, remember that you only can change your first name and last name.'
        return Response(msg, status=st)
    
    def put(self, request, *args, **kwars):
        userid= self.request.user.id
        customer= Customer.objects.get(user__id = userid)

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        try:
            userAccount = request.data.get('user')
            first_name = userAccount['first_name']
            last_name = userAccount['last_name']
        except:
            msg= 'Invalid JSON, the required parameters are: first_name, last_name'
            st=status.HTTP_400_BAD_REQUEST    
            return Response(msg, status=st)
         
        for data in ['first_name', 'last_name']:
            if not data in userAccount:
                st = status.HTTP_400_BAD_REQUEST
                msg = 'Invalid JSON, the required parameters are: first_name, last_name'
                return Response(msg, status=st)

        try:
            with transaction.atomic():
                
                user = User.objects.get(pk = userid )

                if (first_name==None or first_name=='') and (last_name!=None and last_name!='' ):  
                    user.last_name=last_name
                elif (first_name!=None and first_name!='') and (last_name==None or last_name=='' ):   
                    user.first_name=first_name
                elif (first_name!=None and first_name!='') and (last_name!=None and last_name!='' ):
                    user.first_name=first_name
                    user.last_name=last_name     
                else:
                    pass
                    

                user.clean()
                user.save()
                customer.clean()
                customer.save()
            
        except: 
            msg = 'some of atributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY
            return Response(msg, status=st)
        
        st = status.HTTP_200_OK
        msg = 'Updated correctly'
        return Response(msg, status=st)
        
        
    def delete(self, request, *args, **kwars):
        logeed_user_id= request.user.id
        user = User.objects.get(pk = logeed_user_id)

        try: 
            user.is_active=False
            user.save()
        except: 
            msg = 'Cannot delete'  
            st = status.HTTP_409_CONFLICT

        st = status.HTTP_200_OK
        msg = 'Deleted correctly'
        return Response(msg, status=st)

class ShowYourAccount(viewsets.ReadOnlyModelViewSet):
    """
    These are the data of your account stored in Pedroso Wildlife zoo.

    """
    permission_classes = [IsAuthenticatedCustomer]
    serializer_class = CustomerSerializer
    queryset=[]
    
    def list(self, request, *args, **kwargs):
        userid= self.request.user.id
        queryset= Customer.objects.get(user__id = userid)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)

