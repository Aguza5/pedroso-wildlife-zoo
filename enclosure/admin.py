from django.contrib import admin
from .models import Enclosure


class EnclosureAdmin(admin.ModelAdmin):
    list_display = ('identificator','name','is_active')
    search_fields = ('identificator','name','zookeeper__user__username',)
    list_per_page = 10
    list_filter = ('is_active',)



admin.site.register(Enclosure,EnclosureAdmin)