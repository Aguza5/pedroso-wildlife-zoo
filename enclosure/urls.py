from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('enclosure/', views.GetAllEnclosures.as_view(), name='enclosure'),
    path('enclosure/<id>/', views.GetEnclosure.as_view(), name='enclosure2'),
]