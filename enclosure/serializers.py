from rest_framework import serializers
from .models import Enclosure


class EnclosureSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Enclosure
        fields = ('id', 'identificator', 'name')



