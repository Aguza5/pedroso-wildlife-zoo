# Generated by Django 3.0.3 on 2020-05-07 10:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('enclosure', '0002_auto_20200503_1634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enclosure',
            name='identificator',
            field=models.TextField(max_length=50, unique=True, verbose_name='Identificator'),
        ),
    ]
