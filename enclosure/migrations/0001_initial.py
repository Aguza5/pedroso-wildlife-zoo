# Generated by Django 3.0.3 on 2020-05-03 16:30

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Enclosure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identificator', models.TextField(max_length=50, verbose_name='Id')),
                ('name', models.CharField(max_length=50, validators=[django.core.validators.MinLengthValidator(4)], verbose_name='Name')),
                ('is_active', models.BooleanField(default=True, verbose_name='Active')),
            ],
        ),
    ]
