from datetime import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Enclosure
from animal.models import Animal
from .serializers import EnclosureSerializer
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class GetAllEnclosures(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticatedVet]
    queryset = Enclosure.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    serializer_class = EnclosureSerializer    

    def get(self, request, *args, **kwargs):
        self.serializer_class = EnclosureSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)


class GetEnclosure(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EnclosureSerializer
    queryset = Enclosure.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = EnclosureSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Enclosure.objects.get(id = id, is_active=True)
        except:
            return Response({"The requested enclosure does not exist"}, status=status.HTTP_400_BAD_REQUEST)
    
        
        # try: 
        #     accountId=request.user.id
        #     vet=Vet.objects.get(user=accountId)
        # except:   
        #     return Response({}, status=status.HTTP_401_UNAUTHORIZED)
  
        return super().get(request, *args, **kwargs) 




       



                


