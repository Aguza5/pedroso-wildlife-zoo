from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from datetime import date
from django.core.validators import MinLengthValidator




class Enclosure(models.Model):
    identificator = models.TextField(('Identificator'),max_length=50, blank=False, null=False)
    name = models.CharField(('Name'),max_length=50,blank=False, null=False, validators=[MinLengthValidator(4)])
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)

    def __str__(self):
       return self.name
    class Meta:
        unique_together = (('identificator','is_active'),)
        
            
  




    
    
       
       