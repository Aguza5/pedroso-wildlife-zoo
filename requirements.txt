Django==3.0.3
psycopg2==2.8.4
djangorestframework==3.11.0
djangorestframework-simplejwt==4.4.0