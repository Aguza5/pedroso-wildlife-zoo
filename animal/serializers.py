from rest_framework import serializers
from .models import Animal
from enclosure.models import Enclosure
from specie.models import Specie
from specie.serializers import SpecieSerializer
from enclosure.serializers import EnclosureSerializer


class AnimalSerializer(serializers.HyperlinkedModelSerializer):
    specie = SpecieSerializer(many=False)
    enclosure = EnclosureSerializer(many=False)

    class Meta:
        model = Animal
        fields = ('id', 'identificator', 'name', 'birthdate', 'sex', 'enclosure', 'specie')

class SpecieSerializerForPost(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Specie
        fields = ('specie',)

class EnclosureSerializerForPost(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Enclosure
        fields = ('identificator',)       

class AnimalSerializerForPost(serializers.HyperlinkedModelSerializer):
    specie = SpecieSerializerForPost(many=False)
    enclosure = EnclosureSerializerForPost(many=False)

    class Meta:
        model = Animal
        fields = ('id', 'identificator', 'name', 'birthdate', 'sex', 'enclosure', 'specie')

