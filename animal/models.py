from django.db import models
from django.core.exceptions import ValidationError
from enclosure.models import Enclosure
from specie.models import Specie
from incompatibility.models import Incompatibility
from django.core.validators import MinLengthValidator
# from datetime import date
from django.core.exceptions import ValidationError
from django.db.models import Q
import datetime

SEX_OPTIONS = (
        ('M', 'Male'),
        ('F','Female'),
        ('M','Hermaphrodite')
    )

class Animal(models.Model):
    identificator = models.CharField(('Identificator'), max_length=200, blank=False, null=False)
    name = models.CharField(('Name'),max_length=50,blank=True, null=True, validators=[MinLengthValidator(4)])
    birthdate = models.DateField(('Birthdate'),blank=False, null=False)
    sex = models.CharField(('Sex'),max_length=1,choices=SEX_OPTIONS,blank=False, null=False)
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    enclosure = models.ForeignKey('enclosure.Enclosure', related_name='relatedenclosureAnimal', on_delete=models.CASCADE)
    specie = models.ForeignKey('specie.Specie', related_name='relatedSpecieAnimal', on_delete=models.CASCADE)

    def __str__(self):
       return self.name
    
    def clean(self):

        format_str = '%Y-%m-%d'
        birhtdate = datetime.datetime.strptime(str(self.birthdate), format_str) 
        
        if birhtdate > datetime.datetime.now():
            raise ValidationError("Date cannot be in the future")


        enclosure = self.enclosure
        specieToAdd = self.specie  

        animalsInEnclosure = Animal.objects.filter(enclosure = enclosure, is_active=True)

        for animalInEnclosure in animalsInEnclosure:

            specie = animalInEnclosure.specie
            incompatibilities = Incompatibility.objects.filter(Q(specie1 = specieToAdd, specie2 = specie, is_active=True) | Q(specie1 = specie, specie2 = specieToAdd, is_active=True))

            if len(incompatibilities) > 0:
                raise ValidationError('There is an incompatibility with this specie in this enclosure')

    class Meta:
        unique_together = (('identificator','is_active'),)    
        
            
  




    
    
       
       