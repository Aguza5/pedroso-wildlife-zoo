from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('animal/', views.GetAllAnimals.as_view(), name='animal'),
    path('animal/<id>/', views.GetAnimal.as_view(), name='animal2'),
    path('animal/enclosure/<id>/', views.AnimalsByEnclosure.as_view(), name='animal3'),
    path('animal/specie/<id>/', views.AnimalsBySpecie.as_view(), name='animal4'),
]