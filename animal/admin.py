from django.contrib import admin
from .models import Animal



class AnimalAdmin(admin.ModelAdmin):
    list_display = ('identificator','name','birthdate','is_active')
    search_fields = ('identificator','name','enclosure__name','specie__specie',)
    list_per_page = 10

    def custom_titled_filter(title):
        class Wrapper(admin.FieldListFilter):
            def __new__(cls, *args, **kwargs):
                instance = admin.FieldListFilter.create(*args, **kwargs)
                instance.title = title
                return instance
        return Wrapper

    list_filter = (('enclosure__name', custom_titled_filter('Enclouser name')),'specie__specie','is_active',)




admin.site.register(Animal,AnimalAdmin)