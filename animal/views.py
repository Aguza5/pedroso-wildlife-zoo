from datetime import datetime
from django.conf import settings
from django.utils import timezone
from django.db.models import Q
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Animal
from specie.models import Specie
from enclosure.models import Enclosure
from vaccine.models import Vaccine
from vet.models import Vet
from incompatibility.models import Incompatibility
from administrator.models import Administrator
from medical_report.models import MedicalReport
from .serializers import AnimalSerializer
from .serializers import AnimalSerializerForPost
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class GetAllAnimals(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticatedVet]
    queryset = Animal.objects.filter(is_active=True)
    allowed_methods = ('GET','POST')
    serializer_class = AnimalSerializer    

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AnimalSerializer
        else:
            return AnimalSerializerForPost

    def get(self, request, *args, **kwargs):
        self.serializer_class = AnimalSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='Ok, created'

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        
        for data in ['identificator', 'name', 'birthdate', 'sex', 'enclosure', 'specie']:
            if not data in request.data:
                return Response({"Some attributes were not provided"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        try: 
            specieIn=request.data.get('specie')
            specieId=specieIn['specie']
            specie = Specie.objects.get(specie = specieId, is_active=True)  
        except:   
            return Response({"Specie invalid"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            enclosureIn=request.data.get('enclosure')
            enclosureIdentificator=enclosureIn['identificator']
            enclosure = Enclosure.objects.get(identificator = enclosureIdentificator, is_active=True)  
        except:   
            return Response({"Enclosure invalid"}, status=status.HTTP_400_BAD_REQUEST)

        
        enclosureIn=request.data.get('enclosure')
        enclosureIdentificator=enclosureIn['identificator']
        enclosure = Enclosure.objects.get(identificator = enclosureIdentificator, is_active=True)  

        specieIn=request.data.get('specie')
        specieId=specieIn['specie']
        specieToAdd = Specie.objects.get(specie = specieId, is_active=True)  

        animalsInEnclosure = Animal.objects.filter(enclosure = enclosure, is_active=True)

        for animalInEnclosure in animalsInEnclosure:

            specie = animalInEnclosure.specie
            incompatibilities = Incompatibility.objects.filter(Q(specie1 = specieToAdd, specie2 = specie, is_active=True) | Q(specie1 = specie, specie2 = specieToAdd, is_active=True))

            if len(incompatibilities) > 0:
                return Response({"There is an incompatibility with this specie in this enclosure"}, status=status.HTTP_400_BAD_REQUEST) 


        
        try:
            animal = Animal(
                identificator=request.data.get('identificator'),
                name=request.data.get('name'),
                birthdate=request.data.get('birthdate'),
                sex=request.data.get('sex'),
                enclosure=enclosure,
                specie=specieToAdd)

            animal.clean()
            animal.full_clean()
            animal.save()
             
        except:
            msg = 'Some attributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class GetAnimal(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AnimalSerializer
    queryset = Animal.objects.filter(is_active=True)
    allowed_methods = ('GET', 'DELETE')
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AnimalSerializer
        else:
            return AnimalSerializerForPost

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = AnimalSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Animal.objects.get(id = id, is_active=True)
        except:
            return Response({"The requested animal does not exist"}, status=status.HTTP_400_BAD_REQUEST)
  
        return super().get(request, *args, **kwargs) 

    def delete(self, request, id):

        try: 
            accountId=request.user.id
            vet=Vet.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a vet"}, status=status.HTTP_401_UNAUTHORIZED)

        animal = self.queryset.get(id=id)

        try: 
            relatedVaccines=Vaccine.objects.get(animal=animal, is_active=True)
            return Response({"You can not delete an animal with related vaccines, please, delete them first"}, status=status.HTTP_409_CONFLICT)
        except:   
            try:
                relatedMedicalReports=MedicalReport.objects.get(animal=animal, is_active=True)
                return Response({"You can not delete an animal with related medical reports, please, delete them first"}, status=status.HTTP_409_CONFLICT)
            except:
                animal.is_active = False
                animal.save() 

        return Response(status=status.HTTP_204_NO_CONTENT)


class AnimalsByEnclosure(generics.ListCreateAPIView):
    serializer_class = AnimalSerializer
    queryset = Animal.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AnimalSerializer
        else:
            return AnimalSerializerForPost

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = AnimalSerializer

        queryset = []

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            enclosure = Enclosure.objects.get(pk = id, is_active=True)
        except:
            return Response({"Invalid Enclosure"}, status=status.HTTP_400_BAD_REQUEST)

        self.queryset = Animal.objects.filter(enclosure = enclosure, is_active=True)
  
        return super().get(request, *args, **kwargs) 

class AnimalsBySpecie(generics.ListCreateAPIView):
    serializer_class = AnimalSerializer
    queryset = Animal.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AnimalSerializer
        else:
            return AnimalSerializerForPost

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = AnimalSerializer

        accountId=str(request.user.id)
        if 'None'==accountId: 
            return Response({"You must be logged"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            specie = Specie.objects.get(pk = id, is_active=True)
        except:
            return Response({"Invalid Specie"}, status=status.HTTP_400_BAD_REQUEST)

        self.queryset = Animal.objects.filter(specie = specie, is_active=True)
        
  
        return super().get(request, *args, **kwargs) 



                


