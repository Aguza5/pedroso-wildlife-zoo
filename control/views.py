from datetime import datetime
from django.conf import settings
from django.utils import timezone
from django.db.models import Q
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Control
from specie.models import Specie
from enclosure.models import Enclosure
from administrator.models import Administrator
from vaccine.models import Vaccine
from zookeeper.models import Zookeeper
from administrator.models import Administrator
from incompatibility.models import Incompatibility
from medical_report.models import MedicalReport
from .serializers import ControlSerializer
from .serializers import ControlSerializerForPost
from datetime import date
from datetime import timedelta
import pytz
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class GetAllControls(generics.ListCreateAPIView):
    # permission_classes = [IsAuthenticatedVet]
    queryset = Control.objects.filter(is_active=True)
    allowed_methods = ('GET', 'POST')
    serializer_class = ControlSerializer    

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ControlSerializer
        else:
            return ControlSerializerForPost

    def get(self, request, *args, **kwargs):
        self.serializer_class = ControlSerializer

        try: 
            accountId=request.user.id
            admin=Administrator.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as an administrator"}, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='Ok, created'

        if request.content_type != "application/json":
            return Response({"Using Html form is not allowed, please use application/json and send the JSON through the panel 'Raw data'"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        
        for data in ['task', 'enclosure']:
            if not data in request.data:
                return Response({"Some attributes were not provided"}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            accountId=request.user.id
            zookeeper=Zookeeper.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a zookeeper"}, status=status.HTTP_401_UNAUTHORIZED)

        try: 
            enclosureIn=request.data.get('enclosure')
            enclosureId=enclosureIn['identificator']
            enclosure = Enclosure.objects.get(identificator = enclosureId, is_active=True)  
        except:   
            return Response({"Enclosure invalid"}, status=status.HTTP_400_BAD_REQUEST)


        
        try:
            control = Control(
                moment=datetime.now(),
                task=request.data.get('task'),
                observation=request.data.get('observation'),
                zookeeper=zookeeper,
                enclosure=enclosure)

            control.full_clean()
            control.save()
             
        except:
            msg = 'Some attributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class GetControl(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ControlSerializer
    queryset = Control.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ControlSerializer
        else:
            return ControlSerializerForPost

    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = ControlSerializer

        try: 
            accountId=request.user.id
            admin=Administrator.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as an administrator"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Control.objects.get(id = id, is_active=True)
        except:
            return Response({"The requested control does not exist"}, status=status.HTTP_400_BAD_REQUEST)
  
        return super().get(request, *args, **kwargs) 

class ControlsByEnclosure(generics.ListCreateAPIView):
    serializer_class = ControlSerializer
    queryset = Control.objects.filter(is_active=True)
    allowed_methods = ('GET',)
    lookup_field = 'id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ControlSerializer
        else:
            return ControlSerializerForPost

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = ControlSerializer

        try: 
            accountId=request.user.id
            admin=Administrator.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as an administrator"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            enclosure = Enclosure.objects.get(id = id, is_active=True)
        except:
            return Response({"Invalid Enclosure"}, status=status.HTTP_400_BAD_REQUEST)

        self.queryset = Control.objects.filter(enclosure = enclosure, is_active=True)
  
        return super().get(request, *args, **kwargs) 

class ControlsByZookeeper(generics.ListCreateAPIView):
    serializer_class = ControlSerializer
    queryset = Control.objects.filter(is_active=True)
    allowed_methods = ('GET',)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ControlSerializer
        else:
            return ControlSerializerForPost

    def get(self, request, *args, **kwargs):
        self.serializer_class = ControlSerializer

        try: 
            accountId=request.user.id
            zookeeper=Zookeeper.objects.get(user=accountId)
        except:   
            return Response({"You must be logged as a zookeeper"}, status=status.HTTP_401_UNAUTHORIZED)

        self.queryset = Control.objects.filter(zookeeper = zookeeper, is_active=True)
  
        return super().get(request, *args, **kwargs) 

       



                


