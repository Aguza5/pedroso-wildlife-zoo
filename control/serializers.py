from rest_framework import serializers
from .models import Control
from enclosure.models import Enclosure
from zookeeper.serializers import ZookeeperSerializer
from enclosure.serializers import EnclosureSerializer


class ControlSerializer(serializers.HyperlinkedModelSerializer):
    zookeeper = ZookeeperSerializer(many=False)
    enclosure = EnclosureSerializer(many=False)

    class Meta:
        model = Control
        fields = ('id','moment', 'task', 'observation', 'enclosure', 'zookeeper')

class EnclosureSerializerForPost(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Enclosure
        fields = ('identificator',)

class ControlSerializerForPost(serializers.HyperlinkedModelSerializer):
    enclosure = EnclosureSerializerForPost(many=False)

    class Meta:
        model = Control
        fields = ('task', 'observation', 'enclosure')



