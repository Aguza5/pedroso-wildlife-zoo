from django.contrib import admin
from .models import Control


class ControlAdmin(admin.ModelAdmin):
    list_display = ('task','get_enclosure','get_zookeeper','is_active')
    search_fields = ('task','enclosure__name','zookeeper__user__username',)
    list_per_page = 10
    list_filter = ('enclosure__name','zookeeper__user__username','is_active',)

    def get_enclosure(self, obj):
            return obj.enclosure
        
    get_enclosure.short_description = 'Enclosure'
    get_enclosure.admin_order_field = 'control__enclosure'

    def get_zookeeper(self, obj):
            return obj.zookeeper
        
    get_zookeeper.short_description = 'Zookeeper'
    get_zookeeper.admin_order_field = 'control__zookeeper'    

admin.site.register(Control,ControlAdmin)