from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('control/zookeeper/', views.ControlsByZookeeper.as_view(), name='control5'),
    path('control/', views.GetAllControls.as_view(), name='control'),
    path('control/<id>/', views.GetControl.as_view(), name='control2'),
    path('control/enclosure/<id>/', views.ControlsByEnclosure.as_view(), name='control3'),
]