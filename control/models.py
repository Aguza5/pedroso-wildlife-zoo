from django.db import models
from django.core.exceptions import ValidationError
from enclosure.models import Enclosure
from zookeeper.models import Zookeeper
from django.core.validators import MinLengthValidator





class Control(models.Model):

    TASK_OPTIONS = (
        ('CL', 'CLEAN'), 
        ('CH','CHECK'),
        ('F', 'FEED')
    )



    moment = models.DateField(('Moment'),blank=False, null=False)
    task = models.CharField(('Task'),blank=False, null=False,choices=TASK_OPTIONS,max_length=2)
    observation = models.CharField(('Observation'),max_length=50,blank=True, null=True)
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    enclosure = models.ForeignKey('enclosure.Enclosure', related_name='relatedenclosureControl', on_delete=models.CASCADE)
    zookeeper = models.ForeignKey('zookeeper.Zookeeper', related_name='relatedZookeeperControl', on_delete=models.CASCADE)

    def __str__(self):
       return self.task
   
        
            
  




    
    
       
       