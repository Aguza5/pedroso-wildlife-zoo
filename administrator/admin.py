from django.contrib import admin
from .models import Administrator




class AdministratorAdmin(admin.ModelAdmin):
    list_display = ('get_user__username','get_user__first_name','get_user__last_name','get_user__is_active')
    search_fields = ('user__username','user__first_name','user__last_name')
    list_per_page = 10
    list_filter = ('user__is_active',)

    def get_user__is_active(self, obj):
        return obj.user.is_active

    get_user__is_active.short_description = 'Is active'
    get_user__is_active.admin_order_field = 'administrator__user__is_active'
    get_user__is_active.boolean = True
    
    def get_user__username(self, obj):
        return obj.user.username

    get_user__username.short_description = 'User name'

    def get_user__first_name(self, obj):
        return obj.user.first_name

    get_user__first_name.short_description = 'Fisrt Name'

    def get_user__last_name(self, obj):
        return obj.user.last_name

    get_user__last_name.short_description = 'Last Name'

admin.site.register(Administrator, AdministratorAdmin)