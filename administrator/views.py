from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .isAuthenticatedAdministrator import IsAuthenticatedAdministrator
from .models import Administrator
from rest_framework import generics, status, viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response
from .serializers import AdministratorSerializer



class ShowYourAccount(viewsets.ReadOnlyModelViewSet):
    """
    This is the information storage in Pedroso Wildlife Zoo Data Base

    """
    permission_classes = [IsAuthenticatedAdministrator]
    serializer_class = AdministratorSerializer
    queryset=[]
    
    def list(self, request, *args, **kwargs):
        userid= self.request.user.id
        queryset= Administrator.objects.get(user__id = userid)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)


def index(request):
    return render(request, 'index.html')