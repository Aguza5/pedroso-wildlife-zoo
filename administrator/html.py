from rest_framework.renderers import BrowsableAPIRenderer


# class NoHTMLFormBrowsableAPIRenderer(BrowsableAPIRenderer):

    # def get_rendered_html_form(self, *args, **kwargs):
    #     """
    #     We don't want the HTML forms to be rendered because it can be
    #     really slow with large datasets
    #     """
    #     return ""

class BrowsableAPIRendererWithoutForms(BrowsableAPIRenderer):
    """Renders the browsable api, but excludes the forms."""
    
    def get_context(self, *args, **kwargs):
        ctx = super().get_context(*args, **kwargs)
        ctx['display_edit_forms'] = True
        return ctx
    # def get_rendered_html_form(self, *args, **kwargs):
    #     return ""     