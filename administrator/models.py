from django.db import models

class Administrator(models.Model):

    user = models.ForeignKey('auth.User', related_name='userAdministrator', on_delete=models.CASCADE)

    def __str__(self):
       return self.user.username
