from . import views
from django.urls import path
from rest_framework import routers
from django.conf.urls import url


router = routers.DefaultRouter()

urlpatterns = [
    path('adminshow/', views.ShowYourAccount.as_view({'get': 'list'}), name='AdminShow'),
    url(r'^$', views.index),
]