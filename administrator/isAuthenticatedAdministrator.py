from rest_framework import permissions
from .models import Administrator

class IsAuthenticatedAdministrator (permissions.BasePermission):
    def has_permission(self, request, view):
    
        user_id = request.user.id
        is_administrator=True
        try:
            Administrator.objects.get(user=user_id)
        except:
            is_administrator=False

        return is_administrator